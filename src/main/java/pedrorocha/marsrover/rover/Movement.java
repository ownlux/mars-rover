package pedrorocha.marsrover.rover;

import pedrorocha.marsrover.rover.exceptions.InvalidMovementCodeException;

public enum Movement {
    ROTATE_LEFT("L"), ROTATE_RIGHT("R"), MOVE_FORWARD("M");

    private final String code;

    Movement(String code) {
        this.code = code;
    }

    public static Movement parse(String value) {
        for(Movement movement : values()) {
            if (movement.code.equals(value)) {
                return movement;
            }
        }
        throw new InvalidMovementCodeException(value);
    }
}
