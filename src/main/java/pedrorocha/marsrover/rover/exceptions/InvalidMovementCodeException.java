package pedrorocha.marsrover.rover.exceptions;

public class InvalidMovementCodeException extends RuntimeException {

    public InvalidMovementCodeException(String code) {
        super("The exceptions code \"" + code + "\" is invalid.");
    }
}
