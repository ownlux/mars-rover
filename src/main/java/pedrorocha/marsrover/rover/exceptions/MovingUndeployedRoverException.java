package pedrorocha.marsrover.rover.exceptions;

import pedrorocha.marsrover.ApplicationException;

public class MovingUndeployedRoverException extends ApplicationException {
    public MovingUndeployedRoverException() {
        super("Moving a undeployed rover.");
    }
}
