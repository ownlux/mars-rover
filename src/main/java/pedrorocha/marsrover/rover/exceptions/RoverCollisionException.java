package pedrorocha.marsrover.rover.exceptions;

import pedrorocha.marsrover.ApplicationException;

public class RoverCollisionException extends ApplicationException {

    public RoverCollisionException() {
        super("Rover collides with another rover.");
    }
}
