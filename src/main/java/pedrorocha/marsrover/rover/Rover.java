package pedrorocha.marsrover.rover;

import pedrorocha.marsrover.cartography.Positioning;
import pedrorocha.marsrover.cartography.exceptions.OutsideAreaBoundariesException;
import pedrorocha.marsrover.rover.exceptions.MovingUndeployedRoverException;
import pedrorocha.marsrover.rover.exceptions.RoverCollisionException;

import java.util.ArrayList;
import java.util.List;

public class Rover {

    private Positioning positioning;
    private List<Movement> movements;
    private boolean deployed;

    public Rover(Positioning positioning) {
        this.positioning = positioning;
        this.movements = new ArrayList<>();
    }

    public void addMovements(List<Movement> movements) {
        this.movements.addAll(movements);
    }

    public void move(List<Rover> rovers) throws OutsideAreaBoundariesException, RoverCollisionException, MovingUndeployedRoverException {
        if (!deployed) {
            throw new MovingUndeployedRoverException();
        }

        for (Movement movement : movements) {
            if (movement.equals(Movement.ROTATE_LEFT)) {
                positioning.rotateLeft();
            }
            if (movement.equals(Movement.ROTATE_RIGHT)) {
                positioning.rotateRight();
            }
            if (movement.equals(Movement.MOVE_FORWARD)) {
                checkCollisions(rovers);
                positioning.moveForward();
            }
        }
    }

    public boolean collides(Rover rover) {
        return !this.equals(rover) && rover.isDeployed() && positioning.collides(rover.getPositioning());
    }

    public void deploy(List<Rover> rovers) throws RoverCollisionException {
        for (Rover deployedRover : rovers) {
            if (collides(deployedRover)) {
                throw new RoverCollisionException();
            }
        }
        deployed = true;
    }

    public boolean isDeployed() {
        return deployed;
    }

    public Positioning getPositioning() {
        return positioning;
    }

    private void checkCollisions(List<Rover> rovers) throws RoverCollisionException {
        for(Rover rover : rovers) {
            if (collides(rover)) {
                throw new RoverCollisionException();
            }
        }
    }

    public List<Movement> getMovements() {
        return movements;
    }
}
