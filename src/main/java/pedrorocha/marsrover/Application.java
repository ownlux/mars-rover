package pedrorocha.marsrover;

import pedrorocha.marsrover.cartography.exceptions.OutsideAreaBoundariesException;
import pedrorocha.marsrover.converters.DirectionConverter;
import pedrorocha.marsrover.converters.MovementsConverter;
import pedrorocha.marsrover.converters.PointConverter;
import pedrorocha.marsrover.converters.RoversConverter;
import pedrorocha.marsrover.filereader.Instruction;
import pedrorocha.marsrover.filereader.InstructionFileReader;
import pedrorocha.marsrover.rover.Rover;
import pedrorocha.marsrover.rover.exceptions.MovingUndeployedRoverException;
import pedrorocha.marsrover.rover.exceptions.RoverCollisionException;

import java.util.List;

public class Application {

    private static final String DEFAULT_FILE_PATH = "src/main/resources/input.txt";

    public static void main(String args[]) {
        String file = getFileName(args);

        try {
            List<Instruction> instructions = InstructionFileReader.read(file);
            List<Rover> rovers = getRovers(instructions);
            deployRovers(rovers);
            moveRovers(rovers);
        } catch (ApplicationException exception) {
            System.out.println(exception.getMessage());
        }
    }

    private static String getFileName(String[] args) {
        String file = DEFAULT_FILE_PATH;
        if (args.length > 0) {
            file = args[0];
        }
        return file;
    }

    private static void moveRovers(List<Rover> rovers) throws RoverCollisionException, MovingUndeployedRoverException, OutsideAreaBoundariesException {
        for(Rover rover : rovers) {
            rover.move(rovers);
            System.out.println(rover.getPositioning());
        }
    }

    private static void deployRovers(List<Rover> rovers) throws RoverCollisionException {
        for(Rover rover : rovers) {
            rover.deploy(rovers);
        }
    }

    private static List<Rover> getRovers(List<Instruction> instructions) {
        PointConverter pointConverter = new PointConverter();
        DirectionConverter directionConverter = new DirectionConverter();
        MovementsConverter movementsConverter = new MovementsConverter();
        RoversConverter roversConverter = new RoversConverter(pointConverter, directionConverter, movementsConverter);
        return roversConverter.convert(instructions);
    }

}
