package pedrorocha.marsrover.converters;

import pedrorocha.marsrover.cartography.Area;
import pedrorocha.marsrover.cartography.Direction;
import pedrorocha.marsrover.cartography.Point;
import pedrorocha.marsrover.cartography.Positioning;
import pedrorocha.marsrover.filereader.Instruction;
import pedrorocha.marsrover.filereader.InstructionType;
import pedrorocha.marsrover.rover.Movement;
import pedrorocha.marsrover.rover.Rover;

import java.util.ArrayList;
import java.util.List;

public class RoversConverter {

    private PointConverter pointConverter;
    private DirectionConverter directionConverter;
    private MovementsConverter movementsConverter;

    public RoversConverter(PointConverter pointConverter, DirectionConverter directionConverter, MovementsConverter movementsConverter) {
        this.pointConverter = pointConverter;
        this.directionConverter = directionConverter;
        this.movementsConverter = movementsConverter;
    }

    public List<Rover> convert(List<Instruction> instructions) {
        List<Rover> rovers = new ArrayList<>();
        Area area = null;
        Rover rover = null;
        for(Instruction instruction : instructions) {
            if (instruction.getType().equals(InstructionType.AREA)) {
                Point point = pointConverter.convert(instruction.getValue());
                area = new Area(point);
            }
            if (instruction.getType().equals(InstructionType.POSITIONING)) {
                Point point = pointConverter.convert(instruction.getValue());
                Direction direction = directionConverter.convert(instruction.getValue());
                Positioning positioning = new Positioning(area, point, direction);
                rover = new Rover(positioning);
            }
            if (instruction.getType().equals(InstructionType.MOVEMENTS)) {
                List<Movement> movements = movementsConverter.convert(instruction.getValue());
                rover.addMovements(movements);
                rovers.add(rover);
            }
        }
        return rovers;
    }
}
