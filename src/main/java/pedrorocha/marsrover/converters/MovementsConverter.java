package pedrorocha.marsrover.converters;

import pedrorocha.marsrover.rover.Movement;

import java.util.ArrayList;
import java.util.List;

public class MovementsConverter {

    private static final String SEPARATOR = "";

    public List<Movement> convert(String instruction) {
        List<Movement> movements = new ArrayList<>();
        String[] instructionParts = instruction.split(SEPARATOR);
        for(String code : instructionParts) {
            Movement movement = Movement.parse(code);
            movements.add(movement);
        }

        return movements;
    }

}
