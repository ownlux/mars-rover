package pedrorocha.marsrover.converters;

import pedrorocha.marsrover.cartography.Direction;

public class DirectionConverter {
    private static final String SEPARATOR = " ";

    public Direction convert(String instruction) {
        String[] instructionParts = instruction.split(SEPARATOR);
        return Direction.parse(instructionParts[2]);
    }
}
