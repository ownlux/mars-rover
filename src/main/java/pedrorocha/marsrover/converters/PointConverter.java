package pedrorocha.marsrover.converters;

import pedrorocha.marsrover.cartography.Point;

public class PointConverter {

    private static final String SEPARATOR = " ";

    public Point convert(String instruction) {
        String[] instructionParts = instruction.split(SEPARATOR);
        return createPoint(instructionParts);
    }

    private Point createPoint(String[] instructionParts) {
        int x = Integer.parseInt(instructionParts[0]);
        int y = Integer.parseInt(instructionParts[1]);
        return new Point(x, y);
    }
}
