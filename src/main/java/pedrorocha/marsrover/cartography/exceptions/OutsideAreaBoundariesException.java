package pedrorocha.marsrover.cartography.exceptions;

import pedrorocha.marsrover.ApplicationException;

public class OutsideAreaBoundariesException extends ApplicationException {
    public OutsideAreaBoundariesException() {
        super("Positioning point is out of area boundaries.");
    }
}
