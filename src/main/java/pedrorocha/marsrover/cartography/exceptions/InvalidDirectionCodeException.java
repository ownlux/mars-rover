package pedrorocha.marsrover.cartography.exceptions;

public class InvalidDirectionCodeException extends RuntimeException {
    public InvalidDirectionCodeException(String code) {
        super("The direction code \"" + code + "\" is invalid.");
    }
}
