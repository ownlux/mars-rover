package pedrorocha.marsrover.cartography;

public class Area {

    private Point size;

    public Area(Point size) {
        this.size = size;
    }

    public boolean contains(Point point) {
        if (point.getX() >= 0 &&
                point.getY() >= 0 &&
                point.getX() <= size.getX() &&
                point.getY() <= size.getY()) {
            return true;
        }

        return false;
    }
}
