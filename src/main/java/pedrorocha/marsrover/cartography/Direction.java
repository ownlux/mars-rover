package pedrorocha.marsrover.cartography;

import pedrorocha.marsrover.cartography.exceptions.InvalidDirectionCodeException;

public enum Direction {
    NORTH("N", new Point(0, 1)),
    EAST("E", new Point(1, 0)),
    SOUTH("S", new Point(0, -1)),
    WEST("W", new Point(-1, 0));

    private final String code;
    private final Point point;

    Direction(String code, Point point) {
        this.code = code;
        this.point = point;
    }

    public Direction getNextClockwiseDirection() {
        Direction[] directions = values();
        int nextIndex = (ordinal() + 1) % directions.length;
        return directions[nextIndex];
    }

    public Direction getNextCounterclockwiseDirection() {
        Direction[] directions = values();
        int nextIndex = (ordinal() + directions.length - 1) % directions.length;
        return directions[nextIndex];
    }

    public static Direction parse(String code) {
        for(Direction direction : values()) {
            if (direction.code.equals(code)) {
                return direction;
            }
        }
        throw new InvalidDirectionCodeException(code);
    }

    public int getX() {
        return this.point.getX();
    }

    public int getY() {
        return this.point.getY();
    }

    @Override
    public String toString() {
        return code;
    }
}
