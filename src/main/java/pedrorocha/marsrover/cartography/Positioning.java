package pedrorocha.marsrover.cartography;

import pedrorocha.marsrover.cartography.exceptions.OutsideAreaBoundariesException;


public class Positioning {

    private Area area;
    private Point point;
    private Direction direction;

    public Positioning(Area area, Point point, Direction direction) {
        this.area = area;
        this.point = point;
        this.direction = direction;
    }

    public void rotateRight() {
        this.direction = this.direction.getNextClockwiseDirection();
    }

    public void rotateLeft() {
        this.direction = this.direction.getNextCounterclockwiseDirection();
    }

    public void moveForward() throws OutsideAreaBoundariesException {
        this.point.translate(direction.getX(), direction.getY());
        if (!area.contains(point)) {
            throw new OutsideAreaBoundariesException();
        }
    }

    @Override
    public String toString() {
        return this.point + " " + this.direction;
    }

    public boolean collides(Positioning positioning) {
        return point.equals(positioning.point);
    }
}
