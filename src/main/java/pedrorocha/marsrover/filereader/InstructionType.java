package pedrorocha.marsrover.filereader;

public enum InstructionType {
    AREA("^\\d+\\s+\\d+\\s{0,}"), POSITIONING("^\\d+\\s+\\d+\\s+[NESW]\\s{0,}$"), MOVEMENTS("^[LRM]{0,}$");

    private String regex;

    InstructionType(String regex) {
        this.regex = regex;
    }

    public String getRegex() {
        return regex;
    }
}
