package pedrorocha.marsrover.filereader;

import pedrorocha.marsrover.filereader.exceptions.InvalidInstructionException;

import java.util.regex.Pattern;

public class Instruction {

    private InstructionType type;
    private String value;

    public Instruction(String value) throws InvalidInstructionException {
        this(null, value);
    }

    public Instruction(Instruction lastInstruction, String value) throws InvalidInstructionException {
        if (lastInstruction == null && isAreaInstructionValid(value)) {
            type = InstructionType.AREA;
        } else if (lastInstruction != null && isPositioningInstructionValid(value)) {
            type = InstructionType.POSITIONING;
        } else if (lastInstruction != null && lastInstruction.getType() == InstructionType.POSITIONING && isMovementsInstructionValid(value)) {
            type = InstructionType.MOVEMENTS;
        } else {
            throw new InvalidInstructionException(value);
        }
        this.value = value;
    }

    public InstructionType getType() {
        return type;
    }

    public String getValue() {
        return value;
    }

    private static boolean isAreaInstructionValid(String value) {
        Pattern pattern = Pattern.compile(InstructionType.AREA.getRegex());
        return pattern.matcher(value).matches();
    }

    private static boolean isPositioningInstructionValid(String value) {
        Pattern pattern = Pattern.compile(InstructionType.POSITIONING.getRegex());
        return pattern.matcher(value).matches();
    }

    private static boolean isMovementsInstructionValid(String value) {
        Pattern pattern = Pattern.compile(InstructionType.MOVEMENTS.getRegex());
        return pattern.matcher(value).matches();
    }

}
