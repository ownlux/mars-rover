package pedrorocha.marsrover.filereader.exceptions;

import pedrorocha.marsrover.ApplicationException;

public class InstructionFileReaderException extends ApplicationException {
    public InstructionFileReaderException(String file) {
        super("Instruction file \"" + file + "\" not found.");
    }
}
