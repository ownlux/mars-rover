package pedrorocha.marsrover.filereader.exceptions;

import pedrorocha.marsrover.ApplicationException;

public class InvalidInstructionException extends ApplicationException {

    public InvalidInstructionException(String instruction) {
        super("Invalid instruction \"" + instruction + "\".");
    }
}
