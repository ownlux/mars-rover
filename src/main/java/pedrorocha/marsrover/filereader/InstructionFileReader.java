package pedrorocha.marsrover.filereader;

import pedrorocha.marsrover.filereader.exceptions.InstructionFileReaderException;
import pedrorocha.marsrover.filereader.exceptions.InvalidInstructionException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class InstructionFileReader {

    public static List<Instruction> read(String file) throws InstructionFileReaderException, InvalidInstructionException {
        List<String> instructionsValues = readFile(file);

        List<Instruction> instructions = new ArrayList<>();
        Instruction instruction = null;
        for(String instructionValue : instructionsValues) {
            instruction = new Instruction(instruction, instructionValue);
            instructions.add(instruction);
        }
        return instructions;
    }

    private static List<String> readFile(String file) throws InstructionFileReaderException {
        try {
            Stream<String> stream = Files.lines(Paths.get(file));
            List<String> instructionsValues = stream.filter(instruction -> ignoreEmptyLines(instruction))
                    .collect(Collectors.toList());
            return instructionsValues;
        } catch (IOException exception) {
            throw new InstructionFileReaderException(file);
        }
    }

    private static boolean ignoreEmptyLines(String instruction) {
        return !"".equals(instruction.trim());
    }
}
