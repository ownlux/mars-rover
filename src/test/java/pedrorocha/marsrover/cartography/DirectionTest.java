package pedrorocha.marsrover.cartography;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import pedrorocha.marsrover.cartography.exceptions.InvalidDirectionCodeException;

import static org.apache.commons.lang.RandomStringUtils.randomAlphanumeric;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class DirectionTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void shouldNorthReturnsEastWhenGetNextClockwiseDirectionIsCalled() {
        assertThat(Direction.NORTH.getNextClockwiseDirection(), is(Direction.EAST));
    }

    @Test
    public void shouldNorthReturnsWestWhenGetNextClockwiseDirectionIsCalled() {
        assertThat(Direction.NORTH.getNextCounterclockwiseDirection(), is(Direction.WEST));
    }

    @Test
    public void shouldEastReturnsSouthWhenGetNextClockwiseDirectionIsCalled() {
        assertThat(Direction.EAST.getNextClockwiseDirection(), is(Direction.SOUTH));
    }

    @Test
    public void shouldEastReturnsNorthWhenGetNextClockwiseDirectionIsCalled() {
        assertThat(Direction.EAST.getNextCounterclockwiseDirection(), is(Direction.NORTH));
    }

    @Test
    public void shouldSouthReturnsWestWhenGetNextClockwiseDirectionIsCalled() {
        assertThat(Direction.SOUTH.getNextClockwiseDirection(), is(Direction.WEST));
    }

    @Test
    public void shouldSouthReturnsEastWhenGetNextClockwiseDirectionIsCalled() {
        assertThat(Direction.SOUTH.getNextCounterclockwiseDirection(), is(Direction.EAST));
    }

    @Test
    public void shouldWestReturnsNorthWhenGetNextClockwiseDirectionIsCalled() {
        assertThat(Direction.WEST.getNextClockwiseDirection(), is(Direction.NORTH));
    }

    @Test
    public void shouldWestReturnsSouthWhenGetNextClockwiseDirectionIsCalled() {
        assertThat(Direction.WEST.getNextCounterclockwiseDirection(), is(Direction.SOUTH));
    }

    @Test
    public void shouldReturnStringRepresentingTheDirectionUsingTheCode() {
        assertThat(Direction.NORTH.toString(), is("N"));
        assertThat(Direction.EAST.toString(), is("E"));
        assertThat(Direction.SOUTH.toString(), is("S"));
        assertThat(Direction.WEST.toString(), is("W"));
    }

    @Test
    public void shouldHaveTheDirectionNorth() {
        assertThat(Direction.parse("N"), is(Direction.NORTH));
    }

    @Test
    public void shouldHaveTheDirectionEast() {
        assertThat(Direction.parse("E"), is(Direction.EAST));
    }

    @Test
    public void shouldHaveTheDirectionSouth() {
        assertThat(Direction.parse("S"), is(Direction.SOUTH));
    }

    @Test
    public void shouldHaveTheDirectionWest() {
        assertThat(Direction.parse("W"), is(Direction.WEST));
    }

    @Test
    public void shouldThrowsExceptionWhenDirectionIsNotRecognized() {
        String code = randomAlphanumeric(2);

        expectedException.expect(InvalidDirectionCodeException.class);
        expectedException.expectMessage(code);

        Direction.parse(code);
    }

    @Test
    public void shouldGetXandGetYFromNorthReturnsDirectionPointXAndYValues() {
        assertThat(Direction.NORTH.getX(), is(0));
        assertThat(Direction.NORTH.getY(), is(1));
    }

    @Test
    public void shouldGetXandGetYFromEastReturnsDirectionPointXAndYValues() {
        assertThat(Direction.EAST.getX(), is(1));
        assertThat(Direction.EAST.getY(), is(0));
    }

    @Test
    public void shouldGetXandGetYFromSouthReturnsDirectionPointXAndYValues() {
        assertThat(Direction.SOUTH.getX(), is(0));
        assertThat(Direction.SOUTH.getY(), is(-1));
    }

    @Test
    public void shouldGetXandGetYFromWestReturnsDirectionPointXAndYValues() {
        assertThat(Direction.WEST.getX(), is(-1));
        assertThat(Direction.WEST.getY(), is(0));
    }
}