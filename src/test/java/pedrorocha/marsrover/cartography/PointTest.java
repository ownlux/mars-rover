package pedrorocha.marsrover.cartography;

import org.junit.Before;
import org.junit.Test;

import java.util.Random;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;

public class PointTest {

    private Random random;

    @Before
    public void setUp() {
        random = new Random();
    }

    @Test
    public void shouldCreateAPointWithXAndY() {
        int x = random.nextInt();
        int y = random.nextInt();

        Point point = new Point(x, y);

        assertThat(point.getX(), is(x));
        assertThat(point.getY(), is(y));
    }

    @Test
    public void shouldTranslateAPointDxAndDyFromOriginalPosition() {
        int x = random.nextInt();
        int y = random.nextInt();
        int dx = random.nextInt();
        int dy = random.nextInt();
        Point point = new Point(x, y);

        point.translate(dx, dy);

        assertThat(point.getX(), is(x+dx));
        assertThat(point.getY(), is(y+dy));
    }

    @Test
    public void shouldReturnAnStringRepresentingThePoint() {
        int x = random.nextInt();
        int y = random.nextInt();
        Point point = new Point(x, y);

        String pointString = point.toString();

        assertThat(pointString, is(x + " " + y));
    }

    @Test
    public void shouldConsiderEqualsPointsWithSameXAndY() {
        int x = random.nextInt(100);
        int y = random.nextInt(100);
        Point pointOne = new Point(x, y);
        Point pointTwo = new Point(x, y);

        assertThat(pointOne, is(pointTwo));
    }

    @Test
    public void shouldConsiderNotEqualsWhenXIsDifferentBetweenTwoPoint() {
        int x = random.nextInt(100);
        int y = random.nextInt(100);
        Point pointOne = new Point(x, y);
        Point pointTwo = new Point(x+1, y);

        assertThat(pointOne, is(not(pointTwo)));
    }

    @Test
    public void shouldReturnsFalseWhenPointToCompareIsNull() {
        int x = random.nextInt();
        int y = random.nextInt();
        Point pointOne = new Point(x, y);
        Point pointTwo = null;

        assertThat(pointOne, is(not(pointTwo)));
    }

    @Test
    public void shouldBuildTheSameHashCodeWhenPointsAsTheSameXAndY() {
        int x = random.nextInt(1000)+1;
        int y = random.nextInt(1000)+1;
        Point pointOne = new Point(x, y);
        int expectedHashCode = x * 31 + y;

        int oneHashCode = pointOne.hashCode();

        assertThat(oneHashCode, is(expectedHashCode));
    }
}