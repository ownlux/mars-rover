package pedrorocha.marsrover.cartography.exceptions;

import org.junit.Test;
import pedrorocha.marsrover.ApplicationException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.Is.isA;

public class OutsideAreaBoundariesExceptionTest {

    @Test
    public void shouldBeAnInstanceOfApplicationException() {
        OutsideAreaBoundariesException exception = new OutsideAreaBoundariesException();

        assertThat(exception, isA(ApplicationException.class));
    }

    @Test
    public void shouldCreateAnExceptionWithCustomMessage() {
        OutsideAreaBoundariesException exception = new OutsideAreaBoundariesException();

        String message = exception.getMessage();

        assertThat(message, is("Positioning point is out of area boundaries."));
    }
}