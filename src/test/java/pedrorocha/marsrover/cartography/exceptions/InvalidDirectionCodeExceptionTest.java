package pedrorocha.marsrover.cartography.exceptions;

import org.junit.Test;

import static org.apache.commons.lang.RandomStringUtils.randomAlphanumeric;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.Is.isA;

public class InvalidDirectionCodeExceptionTest {

    @Test
    public void shouldBeAnInstanceOfRuntimeException() {
        InvalidDirectionCodeException exception = new InvalidDirectionCodeException(randomAlphanumeric(10));

        assertThat(exception, isA(RuntimeException.class));
    }

    @Test
    public void shouldCreateAnExceptionWithCustomMessage() {
        String code = randomAlphanumeric(10);
        InvalidDirectionCodeException exception = new InvalidDirectionCodeException(code);

        String message = exception.getMessage();

        assertThat(message, is("The direction code \"" + code + "\" is invalid."));
    }
}