package pedrorocha.marsrover.cartography;

import org.junit.Before;
import org.junit.Test;
import pedrorocha.marsrover.cartography.exceptions.OutsideAreaBoundariesException;

import java.util.Random;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PositioningTest {

    private Area area;
    private Point point;
    private Direction direction;
    private Random random;

    @Before
    public void setUp() {
        random = new Random();
        area = mock(Area.class);
        point = mock(Point.class);
        direction = randomDirection();
    }

    @Test
    public void shouldCreatesARoverWithPointDirectionAndSpeed() {
        Positioning positioning = new Positioning(area, point, direction);

        assertThat(positioning.toString(), is(point + " " + direction));
    }

    @Test
    public void shouldRoverRotateRight() {
        Direction rightDirection = direction.getNextClockwiseDirection();
        Positioning positioning = new Positioning(area, point, direction);

        positioning.rotateRight();

        assertThat(positioning.toString(), is(point + " " + rightDirection));
    }

    @Test
    public void shouldRoverRotateLeft() {
        Direction leftDirection = direction.getNextCounterclockwiseDirection();
        Positioning positioning = new Positioning(area, point, direction);

        positioning.rotateLeft();

        assertThat(positioning.toString(), is(point + " " + leftDirection));
    }

    @Test
    public void shouldMultiplySpeedByDirectionPointXWhenTranslating() throws OutsideAreaBoundariesException {
        when(area.contains(point)).thenReturn(true);
        Direction direction = Direction.EAST;
        Positioning positioning = new Positioning(area, point, direction);

        positioning.moveForward();

        verify(point).translate(eq(direction.getX()), anyInt());
    }

    @Test
    public void shouldMultiplySpeedByDirectionPointYWhenTranslating() throws OutsideAreaBoundariesException {
        when(area.contains(point)).thenReturn(true);
        Direction direction = Direction.NORTH;
        Positioning positioning = new Positioning(area, point, direction);

        positioning.moveForward();

        verify(point).translate(anyInt(), eq(direction.getY()));
    }

    @Test
    public void shouldMoveForward() throws OutsideAreaBoundariesException {
        int x = random.nextInt(100);
        int y = random.nextInt(100);
        when(point.toString()).thenReturn(x + " " + y);
        when(area.contains(point)).thenReturn(true);
        Positioning positioning = new Positioning(area, point, direction);

        positioning.moveForward();

        assertThat(positioning.toString(), is(x + " " + y + " " + direction));
    }

    @Test
    public void shouldReturnsTrueWhenOnePositioningCollidesWithAnother() {
        Point point = mock(Point.class);
        Positioning positioningOne = new Positioning(area, point, randomDirection());
        Positioning positioningTwo = new Positioning(area, point, randomDirection());

        boolean collision = positioningOne.collides(positioningTwo);

        assertThat(collision, is(true));
    }

    @Test
    public void shouldReturnsFalseWhenOnePositioningDoesNotCollidesWithAnother() {
        Point pointOne = mock(Point.class);
        Point pointTwo = mock(Point.class);
        Positioning positioningOne = new Positioning(area, pointOne, randomDirection());
        Positioning positioningTwo = new Positioning(area, pointTwo, randomDirection());

        boolean collision = positioningOne.collides(positioningTwo);

        assertThat(collision, is(false));
    }

    @Test(expected = OutsideAreaBoundariesException.class)
    public void shouldThrowsExceptionWhenMovingForwardBeyondAreaBoundaries() throws OutsideAreaBoundariesException {
        Point point = mock(Point.class);
        when(area.contains(point)).thenReturn(false);
        Positioning positioning = new Positioning(area, point, randomDirection());

        positioning.moveForward();
    }

    private Direction randomDirection() {
        int index = random.nextInt(Direction.values().length);
        return Direction.values()[index];
    }
}