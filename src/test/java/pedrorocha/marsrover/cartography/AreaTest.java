package pedrorocha.marsrover.cartography;

import org.junit.Before;
import org.junit.Test;

import java.util.Random;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.mockito.Mockito.mock;

public class AreaTest {

    private Random random;

    @Before
    public void setUp() {
        random = new Random();
    }

    @Test
    public void shouldCreateAnArea() {
        Point point = mock(Point.class);
        Area area = new Area(point);

        assertThat(area, is(notNullValue()));
    }

    @Test
    public void shouldPointBeValid() {
        Point positionPoint = new Point(10, 10);
        Point boundary = new Point(10, 10);
        Area area = new Area(boundary);

        boolean isValid = area.contains(positionPoint);

        assertThat(isValid, is(true));
    }

    @Test
    public void shouldPoint0And0BeValid() {
        Point positionPoint = new Point(0, 0);
        Point boundary = new Point(10, 10);
        Area area = new Area(boundary);

        boolean isValid = area.contains(positionPoint);

        assertThat(isValid, is(true));
    }

    @Test
    public void shouldPointBeInvalidIfGreaterThenPlateauSizes() {
        Point positionPoint = new Point(11, 11);
        Point boundary = new Point(10, 10);
        Area area = new Area(boundary);

        boolean isValid = area.contains(positionPoint);

        assertThat(isValid, is(false));
    }

    @Test
    public void shouldPointBeInvalidIfIsNegative() {
        Point positionPoint = new Point(-1, -1);
        Point boundary = new Point(10, 10);
        Area area = new Area(boundary);

        boolean isValid = area.contains(positionPoint);

        assertThat(isValid, is(false));
    }
}