package pedrorocha.marsrover;

import org.junit.Test;

import static org.apache.commons.lang.RandomStringUtils.randomAlphanumeric;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.Is.isA;

public class ApplicationExceptionTest {

    @Test
    public void shouldBeAnInstanceOfException() {
        ApplicationException exception = new ApplicationException(randomAlphanumeric(10));

        assertThat(exception, isA(Exception.class));
    }

    @Test
    public void shouldCreateAnExceptionWithCustomMessage() {
        String expectedMessage = randomAlphanumeric(10);
        ApplicationException exception = new ApplicationException(expectedMessage);

        String message = exception.getMessage();

        assertThat(message, is(expectedMessage));
    }
}