package pedrorocha.marsrover.filereader;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;

public class InstructionTypeTest {
    @Test
    public void shouldHaveAllThreTypesOfInstruction() {
        assertThat(InstructionType.AREA, is(notNullValue()));
        assertThat(InstructionType.POSITIONING, is(notNullValue()));
        assertThat(InstructionType.MOVEMENTS, is(notNullValue()));
    }

    @Test
    public void shouldReturnsInstructionAreaRegex() {
        assertThat(InstructionType.AREA.getRegex(), is("^\\d+\\s+\\d+\\s{0,}"));
    }

    @Test
    public void shouldReturnsInstructionPositioningRegex() {
        assertThat(InstructionType.POSITIONING.getRegex(), is("^\\d+\\s+\\d+\\s+[NESW]\\s{0,}$"));
    }

    @Test
    public void shouldReturnsInstructionMovementsRegex() {
        assertThat(InstructionType.MOVEMENTS.getRegex(), is("^[LRM]{0,}$"));
    }
}