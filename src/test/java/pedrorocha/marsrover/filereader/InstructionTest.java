package pedrorocha.marsrover.filereader;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import pedrorocha.marsrover.filereader.exceptions.InvalidInstructionException;

import static org.apache.commons.lang.RandomStringUtils.randomAlphanumeric;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.mock;

public class InstructionTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void shouldCreatesAnInstructionWithValueAndTypeArea() throws InvalidInstructionException {
        String value = "1 1";
        Instruction instruction = new Instruction(value);

        assertThat(instruction.getType(), is(InstructionType.AREA));
        assertThat(instruction.getValue(), is(value));
    }

    @Test
    public void shouldCreatesAnInstructionWithValueAndTypePositioning() throws InvalidInstructionException {
        String value = "1 1 S";
        Instruction instruction = new Instruction(mock(Instruction.class), value);

        assertThat(instruction.getType(), is(InstructionType.POSITIONING));
        assertThat(instruction.getValue(), is(value));
    }

    @Test
    public void shouldCreatesAnInstructionWithValueAndTypeMovements() throws InvalidInstructionException {
        String value = "LRM";
        Instruction lastInstruction = new Instruction(mock(Instruction.class), "1 1 S");
        Instruction instruction = new Instruction(lastInstruction, value);

        assertThat(instruction.getType(), is(InstructionType.MOVEMENTS));
        assertThat(instruction.getValue(), is(value));
    }

    @Test
    public void shouldThrowsInvalidInstructionExceptionWhenInstructionIsInvalid() throws InvalidInstructionException {
        String value = randomAlphanumeric(20);

        expectedException.expect(InvalidInstructionException.class);
        expectedException.expectMessage(value);

        new Instruction(value);
    }

    @Test
    public void shouldThrowsInvalidInstructionExceptionWhenAreaInstructionIsNotTheFirst() throws InvalidInstructionException {
        String value = "1 1";
        expectedException.expect(InvalidInstructionException.class);
        expectedException.expectMessage(value);

        new Instruction(mock(Instruction.class), value);
    }

    @Test
    public void shouldThrowsInvalidInstructionExceptionWhenPositioningInstructionIsTheFirst() throws InvalidInstructionException {
        String value = "1 1 N";
        expectedException.expect(InvalidInstructionException.class);
        expectedException.expectMessage(value);

        new Instruction(value);
    }

    @Test
    public void shouldThrowsInvalidInstructionExceptionWhenMovementsInstructionIsNotAfterAnPositioningInstruction() throws InvalidInstructionException {
        String value = "LMLMRM";
        Instruction areaInstruction = new Instruction("1 1");
        expectedException.expect(InvalidInstructionException.class);
        expectedException.expectMessage(value);

        new Instruction(areaInstruction, value);
    }

}