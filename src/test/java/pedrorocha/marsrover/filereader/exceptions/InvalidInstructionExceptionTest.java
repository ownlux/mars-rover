package pedrorocha.marsrover.filereader.exceptions;

import org.junit.Test;
import pedrorocha.marsrover.ApplicationException;

import static org.apache.commons.lang.RandomStringUtils.randomAlphabetic;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.Is.isA;

public class InvalidInstructionExceptionTest {
    @Test
    public void shouldBeAnInstanceOfApplicationException() {
        InvalidInstructionException exception = new InvalidInstructionException(randomAlphabetic(10));

        assertThat(exception, isA(ApplicationException.class));
    }

    @Test
    public void shouldCreateAnExceptionWithCustomMessage() {
        String instruction = randomAlphabetic(10);
        InvalidInstructionException exception = new InvalidInstructionException(instruction);

        String message = exception.getMessage();

        assertThat(message, is("Invalid instruction \"" + instruction + "\"."));
    }
}