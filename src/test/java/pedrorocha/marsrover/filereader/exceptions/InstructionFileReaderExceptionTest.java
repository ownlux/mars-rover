package pedrorocha.marsrover.filereader.exceptions;

import org.junit.Test;
import pedrorocha.marsrover.ApplicationException;

import static org.apache.commons.lang.RandomStringUtils.randomAlphabetic;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.Is.isA;

public class InstructionFileReaderExceptionTest {
    @Test
    public void shouldBeAnInstanceOfApplicationException() {
        InstructionFileReaderException exception = new InstructionFileReaderException(randomAlphabetic(10));

        assertThat(exception, isA(ApplicationException.class));
    }

    @Test
    public void shouldCreateAnExceptionWithCustomMessage() {
        String instruction = randomAlphabetic(10);
        InstructionFileReaderException exception = new InstructionFileReaderException(instruction);

        String message = exception.getMessage();

        assertThat(message, is("Instruction file \"" + instruction + "\" not found."));
    }
}