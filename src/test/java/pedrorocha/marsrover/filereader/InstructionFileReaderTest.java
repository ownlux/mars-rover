package pedrorocha.marsrover.filereader;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import pedrorocha.marsrover.filereader.exceptions.InstructionFileReaderException;
import pedrorocha.marsrover.filereader.exceptions.InvalidInstructionException;

import java.util.List;

import static org.apache.commons.lang.RandomStringUtils.randomAlphanumeric;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class InstructionFileReaderTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void shouldCreateAnListOfInstructions() throws InvalidInstructionException, InstructionFileReaderException {
        String file = "src/test/resources/filereader/validInputTest.txt";

        List<Instruction> instructions = InstructionFileReader.read(file);

        assertThat(instructions.get(0).getType(), is(InstructionType.AREA));
        assertThat(instructions.get(0).getValue(), is("5 5"));
        assertThat(instructions.get(1).getType(), is(InstructionType.POSITIONING));
        assertThat(instructions.get(1).getValue(), is("1 2 N"));
        assertThat(instructions.get(2).getType(), is(InstructionType.MOVEMENTS));
        assertThat(instructions.get(2).getValue(), is("LMLMLMLMM"));
        assertThat(instructions.get(3).getType(), is(InstructionType.POSITIONING));
        assertThat(instructions.get(3).getValue(), is("3 3 E"));
        assertThat(instructions.get(4).getType(), is(InstructionType.MOVEMENTS));
        assertThat(instructions.get(4).getValue(), is("MMRMMRMRRM"));
    }

    @Test
    public void shouldCreateAnListOfInstructionsIgnoringEmptyLines() throws InvalidInstructionException, InstructionFileReaderException {
        String file = "src/test/resources/filereader/validInputWithEmptyLinesTest.txt";

        List<Instruction> instructions = InstructionFileReader.read(file);

        assertThat(instructions.size(), is(5));
    }

    @Test
    public void shouldThrowsInstructionReaderExceptionWhenInstructionWasNotFound() throws InvalidInstructionException, InstructionFileReaderException {
        String instructionFile = randomAlphanumeric(10);
        expectedException.expect(InstructionFileReaderException.class);
        expectedException.expectMessage(instructionFile);

        InstructionFileReader.read(instructionFile);
    }

}