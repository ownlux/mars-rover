package pedrorocha.marsrover.converters;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import pedrorocha.marsrover.cartography.Direction;
import pedrorocha.marsrover.cartography.Point;
import pedrorocha.marsrover.filereader.Instruction;
import pedrorocha.marsrover.filereader.InstructionType;
import pedrorocha.marsrover.rover.Movement;
import pedrorocha.marsrover.rover.Rover;

import java.util.Arrays;
import java.util.List;

import static org.apache.commons.lang.RandomStringUtils.randomAlphanumeric;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RoversConverterTest {

    @Mock
    private PointConverter pointConverter;

    @Mock
    private DirectionConverter directionConverter;

    @Mock
    private MovementsConverter movementsConverter;

    private RoversConverter roversConverter;

    @Before
    public void setUp() {
        roversConverter = new RoversConverter(pointConverter, directionConverter, movementsConverter);
    }

    @Test
    public void shouldCallPointConverterWhenInstructionIsTypeArea() {
        String value = randomAlphanumeric(10);
        Instruction areaInstruction = mock(Instruction.class);
        when(areaInstruction.getType()).thenReturn(InstructionType.AREA);
        when(areaInstruction.getValue()).thenReturn(value);
        List<Instruction> instructions = Arrays.asList(areaInstruction);

        roversConverter.convert(instructions);

        verify(pointConverter).convert(areaInstruction.getValue());
    }

    @Test
    public void shouldCallPointConverterAndDirectionConverterWhenInstructionIsTypePositioning() {
        String value = randomAlphanumeric(10);
        Instruction positioningInstruction = mock(Instruction.class);
        when(positioningInstruction.getType()).thenReturn(InstructionType.POSITIONING);
        when(positioningInstruction.getValue()).thenReturn(value);
        List<Instruction> instructions = Arrays.asList(positioningInstruction);

        roversConverter.convert(instructions);

        verify(pointConverter).convert(value);
        verify(directionConverter).convert(value);
    }

    @Test
    public void shouldCallMovementsConverterWhenInstructionIsTypeMovements() {
        String value = randomAlphanumeric(10);
        Instruction positioningInstruction = mock(Instruction.class);
        when(positioningInstruction.getType()).thenReturn(InstructionType.POSITIONING);
        Instruction movementsInstruction = mock(Instruction.class);
        when(movementsInstruction.getType()).thenReturn(InstructionType.MOVEMENTS);
        when(movementsInstruction.getValue()).thenReturn(value);
        List<Instruction> instructions = Arrays.asList(positioningInstruction, movementsInstruction);

        roversConverter.convert(instructions);

        verify(movementsConverter).convert(movementsInstruction.getValue());
    }

    @Test
    public void shouldCreateAnRoverUsingInstructions() {
        Instruction areaInstruction = mock(Instruction.class);
        when(areaInstruction.getType()).thenReturn(InstructionType.AREA);
        Instruction positioningInstruction = mock(Instruction.class);
        when(positioningInstruction.getType()).thenReturn(InstructionType.POSITIONING);
        when(positioningInstruction.getValue()).thenReturn(randomAlphanumeric(10));
        Instruction movementsInstruction = mock(Instruction.class);
        when(movementsInstruction.getType()).thenReturn(InstructionType.MOVEMENTS);
        when(movementsInstruction.getValue()).thenReturn("LRM");
        List<Instruction> instructions = Arrays.asList(areaInstruction, positioningInstruction, movementsInstruction);
        Point point = mock(Point.class);
        when(point.toString()).thenReturn("1 1");
        when(pointConverter.convert(anyString())).thenReturn(point);
        Direction direction = Direction.NORTH;
        when(directionConverter.convert(anyString())).thenReturn(direction);
        List<Movement> movements = Arrays.asList(Movement.ROTATE_LEFT, Movement.ROTATE_RIGHT, Movement.MOVE_FORWARD);
        when(movementsConverter.convert(anyString())).thenReturn(movements);

        List<Rover> rovers = roversConverter.convert(instructions);

        assertThat(rovers.size(), is(1));
        assertThat(rovers.get(0).getPositioning().toString(), is("1 1 N"));
        assertThat(rovers.get(0).getMovements(), is(movements));
    }
}