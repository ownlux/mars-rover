package pedrorocha.marsrover.converters;

import org.junit.Before;
import org.junit.Test;
import pedrorocha.marsrover.cartography.Direction;
import pedrorocha.marsrover.cartography.Point;

import java.util.Random;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class PointConverterTest {

    private Random random;

    @Before
    public void setUp() {
        random = new Random();
    }

    @Test
    public void shouldCreatePointFromAreaInstruction() {
        int x = random.nextInt(100);
        int y = random.nextInt(100);
        String instruction = x + " " + y;
        PointConverter pointConverter = new PointConverter();

        Point point = pointConverter.convert(instruction);

        assertThat(point, is(new Point(x, y)));
    }

    @Test
    public void shouldCreatePointFromPositioningInstruction() {
        int x = random.nextInt(100);
        int y = random.nextInt(100);
        String instruction = x + " " + y + " " + Direction.values()[random.nextInt(4)].toString();
        PointConverter pointConverter = new PointConverter();

        Point point = pointConverter.convert(instruction);

        assertThat(point, is(new Point(x, y)));
    }
}