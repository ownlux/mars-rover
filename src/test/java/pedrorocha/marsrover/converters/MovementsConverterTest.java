package pedrorocha.marsrover.converters;

import org.junit.Test;
import pedrorocha.marsrover.rover.Movement;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class MovementsConverterTest {

    @Test
    public void shouldCreateAListOfMovementsFromInstruction() {
        String instruction = "LRM";
        MovementsConverter movementsConverter = new MovementsConverter();

        List<Movement> movements = movementsConverter.convert(instruction);

        assertThat(movements.size(), is(instruction.length()));
        assertThat(movements.get(0), is(Movement.ROTATE_LEFT));
        assertThat(movements.get(1), is(Movement.ROTATE_RIGHT));
        assertThat(movements.get(2), is(Movement.MOVE_FORWARD));
    }

}