package pedrorocha.marsrover.converters;

import org.junit.Before;
import org.junit.Test;
import pedrorocha.marsrover.cartography.Direction;

import java.util.Random;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class DirectionConverterTest {

    private Random random;

    @Before
    public void setUp() {
        random = new Random();
    }

    @Test
    public void shouldCreatePointFromAreaInstruction() {
        int x = random.nextInt(100);
        int y = random.nextInt(100);
        Direction expectedDirection = Direction.values()[random.nextInt(4)];
        String instruction = x + " " + y + " " + expectedDirection.toString();
        DirectionConverter directionConverter = new DirectionConverter();

        Direction direction = directionConverter.convert(instruction);

        assertThat(direction, is(expectedDirection));
    }

}