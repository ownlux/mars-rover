package pedrorocha.marsrover.rover;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import pedrorocha.marsrover.rover.exceptions.InvalidMovementCodeException;

import static org.apache.commons.lang.RandomStringUtils.randomAlphanumeric;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class MovementTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void shouldHaveTheActionTurnLeft() {
        assertThat(Movement.parse("L"), is(Movement.ROTATE_LEFT));
    }

    @Test
    public void shouldHaveTheActionTurnRight() {
        assertThat(Movement.parse("R"), is(Movement.ROTATE_RIGHT));
    }

    @Test
    public void shouldHaveTheActionMoveForward() {
        assertThat(Movement.parse("M"), is(Movement.MOVE_FORWARD));
    }

    @Test
    public void shouldThrowsExceptionWhenActionIsNotRecognized() {
        String invalidActionCode = randomAlphanumeric(2);

        expectedException.expect(InvalidMovementCodeException.class);
        expectedException.expectMessage(invalidActionCode);

        Movement.parse(invalidActionCode);
    }
}