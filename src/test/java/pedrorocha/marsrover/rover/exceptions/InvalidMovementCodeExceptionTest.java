package pedrorocha.marsrover.rover.exceptions;

import org.junit.Test;

import static org.apache.commons.lang.RandomStringUtils.randomAlphanumeric;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.Is.isA;

public class InvalidMovementCodeExceptionTest {

    @Test
    public void shouldBeAnInstanceOfRuntimeException() {
        InvalidMovementCodeException exception = new InvalidMovementCodeException(randomAlphanumeric(10));

        assertThat(exception, isA(RuntimeException.class));
    }

    @Test
    public void shouldCreateAnExceptionWithCustomMessage() {
        String code = randomAlphanumeric(10);
        InvalidMovementCodeException exception = new InvalidMovementCodeException(code);

        String message = exception.getMessage();

        assertThat(message, is("The exceptions code \"" + code + "\" is invalid."));
    }
}