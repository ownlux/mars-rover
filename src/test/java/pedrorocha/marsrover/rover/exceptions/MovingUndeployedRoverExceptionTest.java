package pedrorocha.marsrover.rover.exceptions;

import org.junit.Test;
import pedrorocha.marsrover.ApplicationException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.Is.isA;

public class MovingUndeployedRoverExceptionTest {
    @Test
    public void shouldBeAnInstanceOfApplicationException() {
        MovingUndeployedRoverException exception = new MovingUndeployedRoverException();

        assertThat(exception, isA(ApplicationException.class));
    }

    @Test
    public void shouldCreateAnExceptionWithCustomMessage() {
        MovingUndeployedRoverException exception = new MovingUndeployedRoverException();

        String message = exception.getMessage();

        assertThat(message, is("Moving a undeployed rover."));
    }
}