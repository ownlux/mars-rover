package pedrorocha.marsrover.rover.exceptions;

import org.junit.Test;
import pedrorocha.marsrover.ApplicationException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.Is.isA;

public class RoverCollisionExceptionTest {

    @Test
    public void shouldBeAnInstanceOfApplicationException() {
        RoverCollisionException exception = new RoverCollisionException();

        assertThat(exception, isA(ApplicationException.class));
    }

    @Test
    public void shouldCreateAnExceptionWithCustomMessage() {
        RoverCollisionException exception = new RoverCollisionException();

        String message = exception.getMessage();

        assertThat(message, is("Rover collides with another rover."));
    }
}