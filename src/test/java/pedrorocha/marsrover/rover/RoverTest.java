package pedrorocha.marsrover.rover;

import org.junit.Test;
import pedrorocha.marsrover.cartography.Positioning;
import pedrorocha.marsrover.cartography.exceptions.OutsideAreaBoundariesException;
import pedrorocha.marsrover.rover.exceptions.MovingUndeployedRoverException;
import pedrorocha.marsrover.rover.exceptions.RoverCollisionException;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class RoverTest {

    @Test
    public void shouldCreatesARover() {
        Positioning positioning = mock(Positioning.class);

        Rover rover = new Rover(positioning);

        assertThat(rover, is(notNullValue()));
    }

    @Test
    public void shouldRoverMoveUsingHisActions() throws OutsideAreaBoundariesException, RoverCollisionException, MovingUndeployedRoverException {
        Positioning positioning = mock(Positioning.class);
        List<Movement> movements = Arrays.asList(Movement.ROTATE_LEFT, Movement.ROTATE_RIGHT, Movement.MOVE_FORWARD);
        Rover rover = new Rover(positioning);
        rover.addMovements(movements);
        rover.deploy(Arrays.asList(rover));

        rover.move(Arrays.asList());

        verify(positioning).rotateLeft();
        verify(positioning).rotateRight();
        verify(positioning).moveForward();
    }

    @Test
    public void shouldReturnsTrueWhenTwoRoversHasTheSamePositionPointAndIsDeployed() throws RoverCollisionException {
        Positioning positioningOne = mock(Positioning.class);
        Positioning positioningTwo = mock(Positioning.class);
        when(positioningOne.collides(positioningTwo)).thenReturn(true);
        Rover roverOne = new Rover(positioningOne);
        Rover roverTwo = new Rover(positioningTwo);
        roverTwo.deploy(Arrays.asList(roverTwo));

        boolean collides = roverOne.collides(roverTwo);

        assertThat(collides, is(true));
    }

    @Test
    public void shouldReturnsFalseWhenTwoRoversDoesNotHaveTheSamePositionPoint() {
        Positioning positioningOne = mock(Positioning.class);
        Positioning positioningTwo = mock(Positioning.class);
        when(positioningOne.collides(positioningTwo)).thenReturn(false);
        Rover roverOne = new Rover(positioningOne);
        Rover roverTwo = new Rover(positioningTwo);

        boolean collides = roverOne.collides(roverTwo);

        assertThat(collides, is(false));
    }

    @Test
    public void shouldReturnsNotCollidesWithRoverUndeployed() {
        Positioning positioningOne = mock(Positioning.class);
        Positioning positioningTwo = mock(Positioning.class);
        when(positioningOne.collides(positioningTwo)).thenReturn(true);
        Rover roverOne = new Rover(positioningOne);
        Rover roverTwo = new Rover(positioningTwo);

        boolean collides = roverOne.collides(roverTwo);

        assertThat(collides, is(false));
    }

    @Test
    public void shouldReturnsFalseWhenTryingToCheckCollisionBetweenSameRover() {
        Positioning positioning = mock(Positioning.class);
        when(positioning.collides(positioning)).thenReturn(true);
        Rover rover = new Rover(positioning);

        boolean collides = rover.collides(rover);

        assertThat(collides, is(false));
    }

    @Test(expected = RoverCollisionException.class)
    public void shouldThrowsExceptionWhenRoverMovesToAPositionOccupiedByAnotherRover() throws RoverCollisionException, OutsideAreaBoundariesException, MovingUndeployedRoverException {
        Positioning positioningOne = mock(Positioning.class);
        Positioning positioningTwo = mock(Positioning.class);
        Rover roverOne = new Rover(positioningOne);
        Rover roverTwo = new Rover(positioningTwo);
        roverOne.deploy(Arrays.asList(roverOne));
        roverTwo.deploy(Arrays.asList(roverOne, roverTwo));
        roverOne.addMovements(Arrays.asList(Movement.MOVE_FORWARD));
        when(positioningOne.collides(positioningTwo)).thenReturn(true);

        roverOne.move(Arrays.asList(roverTwo));
    }

    @Test
    public void shouldDeployARover() throws RoverCollisionException {
        Positioning positioning = mock(Positioning.class);
        List<Movement> movements = Arrays.asList();
        Rover rover = new Rover(positioning);

        rover.deploy(Arrays.asList(rover));

        assertThat(rover.isDeployed(), is(true));
    }

    @Test(expected = RoverCollisionException.class)
    public void shouldThrowExceptionWhenRoverCollidesWithAnother() throws RoverCollisionException {
        Positioning positioningOne = mock(Positioning.class);
        Positioning positioningTwo = mock(Positioning.class);
        when(positioningOne.collides(positioningTwo)).thenReturn(true);
        Rover roverOne = new Rover(positioningOne);
        Rover roverTwo = new Rover(positioningTwo);
        roverTwo.deploy(Arrays.asList(roverTwo));

        roverOne.deploy(Arrays.asList(roverOne, roverTwo));
    }

    @Test
    public void shouldReturnsAllRoverMovements() {
        Positioning positioning = mock(Positioning.class);
        List<Movement> movements = Arrays.asList(Movement.ROTATE_LEFT, Movement.ROTATE_RIGHT, Movement.MOVE_FORWARD);
        Rover rover = new Rover(positioning);
        rover.addMovements(movements);

        List<Movement> roverMovements = rover.getMovements();

        assertThat(roverMovements, is(movements));
    }
}