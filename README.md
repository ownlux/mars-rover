# MARS ROVER #

## REQUIREMENTS ##

* java 8

## COMMANDS ##

* To run unit tests: 
```
./gradlew test
```

* To run mutation tests: 
```
./gradlew clean pitest
```

* To build: 
```
./gradlew build
```

* To Run:
```
java -jar build/libs/<jar_file_name>.jar <file_path>
* If file_path was omitted it will look at src/main/resource/input.txt
```

## MARS ROVER PROBLEM ##

A squad of robotic rovers are to be landed by NASA on a plateau on Mars. This plateau, which is curiously rectangular, must be navigated by the rovers so that their on board cameras can get a complete view of the surrounding terrain to send back to Earth.

A positioning's point and location is represented by a combination of x and y coordinates and a letter representing one of the four cardinal compass points. The plateau is divided up into a grid to simplify navigation. An example point might be 0, 0, N, which means the positioning is in the bottom left corner and facing North.

In order to control a positioning, NASA sends a simple string of letters. The possible letters are 'L', 'R' and 'M'. 'L' and 'R' makes the positioning spin 90 degrees left or right respectively, without moving from its current spot. 'M' means move forward one grid point, and maintain the same heading.
Assume that the square directly North from (x, y) is (x, y+1).

##### Input: #####

The first line of input is the upper-right coordinates of the plateau, the lower-left coordinates are assumed to be 0,0.

The rest of the input is information pertaining to the rovers that have been deployed. Each positioning has two lines of input. The first line gives the positioning's point, and the second line is a series of instructions telling the positioning how to explore the plateau.

The point is made up of two integers and a letter separated by spaces, corresponding to the x and y coordinates and the positioning's orientation.

Each positioning will be finished sequentially, which means that the second positioning won't start to move until the first one has finished moving.

##### Output: #####

The output for each positioning should be its final coordinates and heading.


Test Input:
```
5 5
1 2 N
LMLMLMLMM
3 3 E
MMRMMRMRRM
```

Expected Output:
```
1 3 N
5 1 E
```
